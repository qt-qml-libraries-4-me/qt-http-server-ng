#pragma once

#include <QByteArray>
#include <QHash>
#include <QJsonDocument>
#include <QObject>
#include <QSslCertificate>
#include <QSslConfiguration>
#include <QSslKey>
#include <QSslSocket>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTimer>
#include <QVector>

#define QS QStringLiteral
#define QB QByteArrayLiteral

namespace QtHTTP {

using QStringHash    = QHash<QString, QString>;
using QByteArrayHash = QHash<QByteArray, QByteArray>;

QByteArray gzipped (const QByteArray & bytes);

enum class StatusCodes {
    UNKNOWN             =  -1,
    OK                  = 200,
    FOUND               = 302,
    SEE_OTHER           = 303,
    BAD_REQUEST         = 400,
    FORBIDDEN           = 403,
    NOT_FOUND           = 404,
    METHOD_NOT_ALLOWED  = 405,
    INTERNAL_ERROR      = 500,
    NOT_IMPLEMENTED     = 501,
    BAD_GATEWAY         = 502,
    SERVICE_UNAVAILABLE = 503,
};

enum class Compression {
    NONE,
    AUTO,
    FORCED,
};

static const struct {
    const QByteArray ACCEPT              { QB ("accept") };
    const QByteArray ACCEPT_CHARSET      { QB ("accept-charset") };
    const QByteArray ACCEPT_ENCODING     { QB ("accept-encoding") };
    const QByteArray ACCEPT_LANGUAGE     { QB ("accept-language") };
    const QByteArray ACCEPT_RANGES       { QB ("accept-ranges") };
    const QByteArray AUTHORIZATION       { QB ("authorization") };
    const QByteArray CONNECTION          { QB ("connection") };
    const QByteArray CONTENT_DISPOSITION { QB ("content-disposition") };
    const QByteArray CONTENT_ENCODING    { QB ("content-encoding") };
    const QByteArray CONTENT_LENGTH      { QB ("content-length") };
    const QByteArray CONTENT_LANGUAGE    { QB ("content-language") };
    const QByteArray CONTENT_LOCATION    { QB ("content-location") };
    const QByteArray CONTENT_RANGE       { QB ("content-range") };
    const QByteArray CONTENT_TYPE        { QB ("content-type") };
    const QByteArray COOKIE              { QB ("cookie") };
    const QByteArray DATE                { QB ("date") };
    const QByteArray HOST                { QB ("host") };
    const QByteArray KEEP_ALIVE          { QB ("keep-alive") };
    const QByteArray LOCATION            { QB ("location") };
    const QByteArray REFERER             { QB ("referer") };
    const QByteArray SERVER              { QB ("server") };
    const QByteArray SET_COOKIE          { QB ("set-cookie") };
    const QByteArray TRAILER             { QB ("trailer") };
    const QByteArray TRANSFER_ENCODING   { QB ("transfer-encoding") };
    const QByteArray USER_AGENT          { QB ("user-agent") };
} Headers;

static const struct {
    const QByteArray GET    { QB ("GET") };
    const QByteArray POST   { QB ("POST") };
    const QByteArray PUT    { QB ("PUT") };
    const QByteArray DELETE { QB ("DELETE") };
    const QByteArray HEAD   { QB ("HEAD") };
} Commands;

static const struct {
    const QByteArray HTTP_1_0 { QB ("HTTP/1.0") };
    const QByteArray HTTP_1_1 { QB ("HTTP/1.1") };
} Versions;

struct DataPart {
    QByteArrayHash headers          { };
    QByteArray     data             { };
    QByteArray     formDataName     { };
    QByteArray     formDataFilename { };

    virtual void clear (void);
};

struct AbstractFrame : public DataPart {
    QString           toHtml       (void) const;
    QJsonDocument     toJson       (void) const;
    QStringHash       toUrlEncoded (void) const;
    QVector<DataPart> toMultiPart  (void) const;

    void fromHtml       (const QString           & html);
    void fromJson       (const QJsonDocument     & json);
    void fromUrlEncoded (const QStringHash       & args);
    void fromMultiPart  (const QVector<DataPart> & parts);
};

struct Request : public AbstractFrame {
    QByteArray cmd     { };
    QByteArray url     { };
    QByteArray version { };

    void clear (void) final;
};

struct Reply : public AbstractFrame {
    StatusCodes status   { StatusCodes::UNKNOWN };
    Compression compress { Compression::NONE };
    bool        async    { false };

    void clear (void) final;
};

class Client : public QObject {
    Q_OBJECT

public:
    enum ParserStatus {
        WAIT_REQUEST,
        WAIT_HEADERS,
        WAIT_DATA,
        DONE,
        WAIT_SEND,
        MUST_SEND,
        ERROR,
        CLOSED,
    };
    Q_ENUM (ParserStatus)

    QTcpSocket * m_sock     { nullptr };
    QTimer       m_watchdog { };
    QByteArray   m_buffer   { };
    Request      m_request  { };
    Reply        m_reply    { };
    ParserStatus m_status   { WAIT_REQUEST };
    bool         m_handling { false };

public:
    explicit Client (QTcpSocket * parent = nullptr);

    void sendReply (const Request & request, Reply & reply);

signals:
    void requested (const QtHTTP::Request * request, QtHTTP::Reply * reply);

protected:
    void parseRequests (void);
};

class Server : public QTcpServer {
    Q_OBJECT

public:
    explicit Server (QObject * parent = nullptr);

    void setServerName (const QString & name);
    void setSecureMode (const QList<QSslCertificate> & caCertificates, const QList<QSslCertificate> & certificates, const QSslKey & privateKey);

public slots:
    void start (const quint16 port = 80);
    void stop  (void);

signals:
    void started      (const quint16 port);
    void connected    (QtHTTP::Client * client);
    void requested    (QtHTTP::Client * client, const QtHTTP::Request * request, QtHTTP::Reply * reply);
    void disconnected (QtHTTP::Client * client);
    void stopped      (void);

protected:
    void incomingConnection (qintptr handle) final;

private:
    bool                   m_useSsl;
    QSslKey                m_sslKey;
    QSslConfiguration      m_sslConfig;
    QList<QSslCertificate> m_sslCerts;
    QByteArray             m_serverName;
};

} // namespace QtHTTP
