
#include "QtHttpServer.hpp"

/// Qt
#include <QDateTime>
#include <QDebug>
#include <QLocale>
#include <QStringBuilder>
#include <QUrlQuery>

/// STD
#include <array>
#include <functional>

#if QT_VERSION_MAJOR < 6
#   define qoffset int
#   define qhashed uint
#else
#   define qoffset qsizetype
#   define qhashed size_t
#endif

#define Q_CONST(NAME,TYPE,VALUE) \
    static const TYPE & NAME (void) { static const TYPE ret { VALUE }; return ret; }

namespace QtHTTP {

static constexpr int   DELAY     { 60 };
static constexpr char  EQUAL     { '=' };
static constexpr char  SPACE     { ' ' };
static constexpr char  QUOTE     { '"' };
static constexpr char  COLON     { ':' };
static constexpr char  SEMICOLON { ';' };
static constexpr qreal THRESHOLD { 0.85 };

Q_CONST (LOCALE_C,          QLocale,    QS ("C"))
Q_CONST (FMT_DATETIME,      QString,    QS ("ddd, dd MMM yyyy hh:mm:ss t"))
Q_CONST (MIMETYPE_URL,      QByteArray, QB ("application/x-www-form-urlencoded"))
Q_CONST (MIMETYPE_JSON,     QByteArray, QB ("application/json"))
Q_CONST (MIMETYPE_HTML,     QByteArray, QB ("text/html"))
Q_CONST (MIMETYPE_MULTI,    QByteArray, QB ("multipart/form-data"))
Q_CONST (BOUNDARY,          QByteArray, QB ("boundary="))
                                                  Q_CONST (TIMEOUT,           QByteArray, QB ("timeout="))
                                                  Q_CONST (DOUBLE_DASH,       QByteArray, QB ("--"))
                                                  Q_CONST (CRLF,              QByteArray, QB ("\r\n"))
Q_CONST (LAST_PART,         QByteArray, QB ("--\r\n"))
Q_CONST (GZIP,              QByteArray, QB ("gzip"))
Q_CONST (CLOSE,             QByteArray, QB ("close"))
Q_CONST (KEEP_ALIVE,        QByteArray, QB ("keep-alive"))
Q_CONST (GZIP_MAGIC_HEADER, QByteArray, QB ("\x1f"   // magic num low
                                            "\x8b"   // magic num high
                                            "\x08"   // deflate algo used
                                            "\x00"   // header flags unset
                                            "\x00"   // null timestamp byte 1
                                            "\x00"   // null timestamp byte 2
                                            "\x00"   // null timestamp byte 3
                                            "\x00"   // null timestamp byte 4
                                            "\x00"   // compression flags unset
                                            "\x00")) // operating system id unknown

static const struct CRC32Impl {
    static constexpr int SIZE { 256 };
    using LookupTable = std::array<quint32, SIZE>;
    const LookupTable LUT {
        [] (void) -> LookupTable {
            LookupTable ret { };
            for (int n { 0 }; n < SIZE; ++n) {
                quint32 c { quint32 (n) };
                for (int k { 0 }; k < 8; ++k) {
                    c = (((c & 1) != 0)
                         ? (0xEDB88320l ^ (c >> 1))
                         : (c >> 1));
                }
                ret [n] = c;
            }
            return ret;
        } ()
    };

    quint32 checksum (const QByteArray & bytes) const {
        quint32 ret { (0ul ^ 0xFFFFFFFFul) };
        for (const char chr : bytes) {
            ret = (LUT [int ((ret ^ quint32 (chr)) & 0xFFu)] ^ (ret >> 8));
        }
        return (ret ^ 0xFFFFFFFFul);
    }
} CRC32 { };

static QByteArray statusString (const StatusCodes code) {
    static const QByteArray EMPTY { };
    static const QHash<StatusCodes, QByteArray> MAP {
        { StatusCodes::OK,                  QB ("Ok")                 },
        { StatusCodes::SEE_OTHER,           QB ("See Other")          },
        { StatusCodes::BAD_REQUEST,         QB ("Bad Request")        },
        { StatusCodes::FORBIDDEN,           QB ("Forbidden")          },
        { StatusCodes::NOT_FOUND,           QB ("Not Found")          },
        { StatusCodes::METHOD_NOT_ALLOWED,  QB ("Method Not Allowed") },
        { StatusCodes::INTERNAL_ERROR,      QB ("Internal Error")     },
        { StatusCodes::NOT_IMPLEMENTED,     QB ("Not Implemented")    },
        { StatusCodes::BAD_GATEWAY,         QB ("Bad Gateway")        },
        { StatusCodes::SERVICE_UNAVAILABLE, QB ("Service Unvailable") },
    };
    return MAP.value (code, EMPTY);
}

inline QByteArray cutBeginEnd (const QByteArray & str, const int charsAtBegin, const int charsAtEnd) {
    return str.mid (charsAtBegin, str.length () - charsAtBegin - charsAtEnd);
}

inline QByteArray unquote (const QByteArray & str) {
    return (str.startsWith (QUOTE) && str.endsWith (QUOTE) ? cutBeginEnd (str, 1, 1) : str);
}

inline qhashed qHash (const QtHTTP::StatusCodes key, const qhashed seed = 0) {
    return ::qHash (int (key), seed);
}

template<typename T> inline QByteArray numAsByteArray (const T value) {
    return QByteArray { reinterpret_cast<const char *> (&value), sizeof (value) };
}

QByteArray gzipped (const QByteArray & bytes) {
    return (QByteArray {
                GZIP_MAGIC_HEADER () // 10-bytes gzip headers
                % cutBeginEnd (qCompress (bytes, 7), 6, 4) // bytes compressed with deflate algo
                % numAsByteArray<quint32> (CRC32.checksum (bytes)) // crc 32 adler
                % numAsByteArray<quint32> (quint32 (bytes.length ()) % 4294967296ull) // size mod 2^32
            });
}

void DataPart::clear (void) {
    data.clear ();
    headers.clear ();
    formDataName.clear ();
    formDataFilename.clear ();
}

void Request::clear (void) {
    DataPart::clear ();
    cmd.clear ();
    url.clear ();
    version.clear ();
}

void Reply::clear (void) {
    DataPart::clear ();
    async    = false;
    status   = StatusCodes::UNKNOWN;
    compress = Compression::NONE;
}

Client::Client (QTcpSocket * parent)
    : QObject { parent }
    , m_sock  { parent }
{
    m_watchdog.setTimerType  (Qt::CoarseTimer);
    m_watchdog.setSingleShot (true);
    connect (&m_watchdog, &QTimer::timeout, this, [this] (void) {
        //qDebug () << ">>> [QtHTTP] Auto-closing persistent socket after 30s timeout !";
        m_sock->close ();
        m_status = CLOSED;
    });
    connect (m_sock, &QTcpSocket::readyRead, this, [this] (void) {
        m_watchdog.stop ();
        m_buffer.append (m_sock->readAll ());
        parseRequests ();
    });
    m_watchdog.start (DELAY * 1000);
}

void Client::parseRequests (void) {
    while (!m_buffer.isEmpty ()) {
        qoffset offset { 0 };
        if (m_status == WAIT_REQUEST) {
            m_request.clear ();
            m_reply.clear ();
            const qoffset posCrlf { m_buffer.indexOf (CRLF (), offset) };
            if (posCrlf > offset) { // command line
                const QByteArrayList line { m_buffer.mid (offset, (posCrlf - offset)).split (SPACE) };
                offset = (posCrlf +2);
                m_request.cmd     = line.value (0);
                m_request.url     = line.value (1);
                m_request.version = line.value (2);
                if ((m_request.cmd == Commands.GET  ||
                     m_request.cmd == Commands.POST ||
                     m_request.cmd == Commands.PUT  ||
                     m_request.cmd == Commands.HEAD ||
                     m_request.cmd == Commands.DELETE) &&
                    !m_request.url.isEmpty () &&
                    (m_request.version == Versions.HTTP_1_0 ||
                     m_request.version == Versions.HTTP_1_1)) {
                    m_status = WAIT_HEADERS;
                }
                else {
                    m_status = ERROR;
                }
            }
        }
        while (m_status == WAIT_HEADERS) {
            const qoffset posCrlf { m_buffer.indexOf (CRLF (), offset) };
            if (posCrlf > offset) { // header line
                const QByteArray line { m_buffer.mid (offset, (posCrlf - offset)) };
                offset = (posCrlf +2);
                const qoffset posColon { line.indexOf (COLON) };
                if (posColon > 0) {
                    m_request.headers.insert (line.left (posColon).trimmed ().toLower (), line.mid (posColon +1).trimmed ());
                    m_status = WAIT_HEADERS;
                }
                else {
                    m_status = ERROR;
                    break;
                }
            }
            else if (posCrlf == offset) { // end of headers
                offset = (posCrlf +2);
                m_status = WAIT_DATA;
            }
            else {
                break;
            }
        }
        if (m_status == WAIT_DATA) {
            const QByteArray contentType   { m_request.headers.value (Headers.CONTENT_TYPE) };
            const QByteArray contentLength { m_request.headers.value (Headers.CONTENT_LENGTH) };
            if (!contentType.isEmpty () && !contentLength.isEmpty () ) {
                const int size { contentLength.toInt () };
                if (size > 0) { // data
                    if (m_buffer.length () >= (offset + size)) {
                        m_request.data = m_buffer.mid (offset, size);
                        offset += size;
                        m_status = DONE;
                    }
                }
            }
            else { // no data
                m_status = DONE;
            }
        }
        if (m_status == DONE) {
            m_handling = true;
            emit requested (&m_request, &m_reply); // handler
            m_handling = false;
            if (m_reply.async) {
                m_status = WAIT_SEND;
            }
            else {
                m_status = MUST_SEND;
                sendReply (m_request, m_reply);
            }
        }
        if (m_status == ERROR) {
            m_buffer.clear ();
            offset = 0;
            m_status = WAIT_REQUEST;
        }
        else {
            if (offset > 0) {
                m_buffer.remove (0, offset);
            }
            else {
                break;
            }
        }
        if (m_status == WAIT_SEND) {
            break; // exit for now
        }
    }
}

void Client::sendReply (const Request & request, Reply & reply) {
    if (m_handling) {
        qDebug () << ">>> [QtHTTP] Replying too early, skipping...";
        return;
    }
    if (m_status != MUST_SEND && m_status != WAIT_SEND) {
        qWarning () << ">>> [QtHTTP] Replying to client in incorrect state !" << m_status;
        return;
    }
    if (reply.status == QtHTTP::StatusCodes::UNKNOWN) {
        qWarning () << ">>> [QtHTTP] Replying to client with unknown status code !";
    }
    reply.headers.insert (Headers.DATE, LOCALE_C ().toString (QDateTime::currentDateTimeUtc (), FMT_DATETIME ()).toUtf8 ());
    if (request.version == Versions.HTTP_1_1) {
        reply.headers.insert (Headers.CONNECTION, KEEP_ALIVE ());
        reply.headers.insert (Headers.KEEP_ALIVE, TIMEOUT () % QByteArray::number (DELAY));
    }
    if (reply.compress != Compression::NONE) {
        if (request.headers.value (Headers.ACCEPT_ENCODING).contains (GZIP ())) {
            const QByteArray rawBytes  { reply.data };
            const QByteArray gzipBytes { gzipped (rawBytes) };
            if (reply.compress == Compression::FORCED || gzipBytes.length () < qoffset (qreal (rawBytes.length ()) * THRESHOLD)) {
                reply.data = gzipBytes;
                reply.headers.insert (Headers.CONTENT_ENCODING, GZIP ());
            }
        }
    }
    reply.headers.insert (Headers.CONTENT_LENGTH, QByteArray::number (reply.data.length ()));
    m_sock->write   (request.version); // reply
    m_sock->putChar (SPACE);
    m_sock->write   (QByteArray::number (int (reply.status)));
    m_sock->putChar (SPACE);
    m_sock->write   (statusString (reply.status));
    m_sock->write   (CRLF ());
    for (QByteArrayHash::const_iterator it { reply.headers.constBegin () }, end { reply.headers.constEnd () }; it != end; ++it) {
        m_sock->write   (it.key ());
        m_sock->putChar (COLON);
        m_sock->putChar (SPACE);
        m_sock->write   (it.value ());
        m_sock->write   (CRLF ());
    }
    m_sock->write (CRLF ());
    m_sock->write (reply.data);
    m_sock->flush ();
    if (request.headers.value (Headers.CONNECTION).toLower () == CLOSE ()) {
        m_sock->close ();
        m_status = CLOSED;
    }
    else {
        m_status = WAIT_REQUEST;
        m_watchdog.start (DELAY * 1000);
        if (!m_handling) {
            QMetaObject::invokeMethod (this, &Client::parseRequests, Qt::QueuedConnection); // in case there are pending requests in buffer
        }
    }
}

Server::Server (QObject * parent) : QTcpServer { parent }, m_useSsl { false } {
    connect (this, &Server::newConnection, this, [this] (void) {
        while (hasPendingConnections ()) {
            if (QTcpSocket * sock { nextPendingConnection () }) {
                Client * client { new Client { sock } };
                if (QSslSocket * ssl { qobject_cast<QSslSocket *> (sock) }) {
                    ssl->setSslConfiguration (m_sslConfig);
                    ssl->setLocalCertificateChain (m_sslCerts);
                    ssl->setPrivateKey (m_sslKey);
                    ssl->setProtocol (QSsl::TlsV1_2OrLater);
                    ssl->setPeerVerifyMode (QSslSocket::VerifyNone);
                    ssl->startServerEncryption ();
                }
                connect (sock, &QTcpSocket::disconnected, this, [this, sock, client] (void) {
                    emit disconnected (client);
                    sock->deleteLater ();
                });
                connect (client, &Client::requested, this, [this, client] (const Request * request, Reply * reply) {
                    reply->headers.insert (Headers.SERVER, m_serverName);
                    emit requested (client, request, reply);
                });
                emit connected (client);
            }
        }
    });
}

void QtHTTP::Server::setServerName (const QString & name) {
    m_serverName = name.toLatin1 ();
}

void QtHTTP::Server::setSecureMode (const QList<QSslCertificate> & caCertificates, const QList<QSslCertificate> & certificates, const QSslKey & privateKey) {
    m_useSsl   = true;
    m_sslKey   = privateKey;
    m_sslCerts = certificates;
    QSslConfiguration sslConfig { QSslConfiguration::defaultConfiguration () };
    QList<QSslCertificate> caCertificatesList { sslConfig.caCertificates () };
    caCertificatesList << caCertificates;
    sslConfig.setCaCertificates (caCertificatesList);
    m_sslConfig = sslConfig;
}

void Server::start (const quint16 port) {
    if (listen (QHostAddress::Any, port)) {
        emit started (serverPort ());
    }
}

void Server::stop (void) {
    if (isListening ()) {
        close ();
        if (!isListening ()) {
            emit stopped ();
        }
    }
}

void Server::incomingConnection (qintptr handle) {
    if (QTcpSocket * sock { (m_useSsl ? new QSslSocket { this } : new QTcpSocket { this }) }) {
        if (sock->setSocketDescriptor (handle)) {
            addPendingConnection (sock);
        }
        else {
            delete sock;
        }
    }
}

QString QtHTTP::AbstractFrame::toHtml (void) const {
    return QString::fromUtf8 (data);
}

QJsonDocument AbstractFrame::toJson (void) const {
    return QJsonDocument::fromJson (data);
}

QStringHash AbstractFrame::toUrlEncoded (void) const {
    QStringHash ret { };
    const QUrlQuery query { QString::fromLatin1 (data) };
    const QList<QPair<QString, QString>> pairsList { query.queryItems (QUrl::FullyDecoded) };
    for (const QPair<QString, QString> & pair : pairsList) {
        ret.insert (pair.first, pair.second);
    }
    return ret;
}

QVector<DataPart> AbstractFrame::toMultiPart (void) const {
    QVector<DataPart> ret { };
    const QByteArray contentType { headers.value (Headers.CONTENT_TYPE) };
    const qoffset posBoundary { contentType.indexOf (BOUNDARY ()) };
    const QByteArray boundary { (DOUBLE_DASH () % unquote (contentType.mid (posBoundary + BOUNDARY ().size ()))) };
    enum Status { WAIT_DELIMITER, WAIT_HEADERS, WAIT_DATA, DONE, ERROR } status { WAIT_DELIMITER };
    if (!boundary.isEmpty ()) {
        qoffset offset { 0 };
        while (offset < data.length ()) {
            DataPart part { };
            if (status == WAIT_DELIMITER) {
                if (data.mid (offset, boundary.size ()) == boundary) {
                    if (data.mid (offset + boundary.size (), CRLF ().size ()) == CRLF ()) {
                        offset = (offset + boundary.size () + CRLF ().size ());
                        status = WAIT_HEADERS;
                    }
                    else if (data.mid (offset + boundary.size (), LAST_PART ().size ()) == LAST_PART ()) {
                        offset = (offset + boundary.size () + LAST_PART ().size ());
                        status = DONE;
                        break;
                    }
                    else {
                        status = ERROR;
                    }
                }
                else {
                    status = ERROR;
                }
            }
            while (status == WAIT_HEADERS) {
                const qoffset posCrlf { data.indexOf (CRLF (), offset) };
                if (posCrlf > offset) { // header line
                    const QByteArray line { data.mid (offset, (posCrlf - offset)) };
                    offset = (posCrlf +2);
                    const qoffset posColon { line.indexOf (COLON) };
                    if (posColon > 0) {
                        const QByteArray headerKey { line.left (posColon).trimmed ().toLower () };
                        const QByteArray headerVal { line.mid (posColon +1).trimmed () };
                        part.headers.insert (headerKey, headerVal);
                        if (headerKey == Headers.CONTENT_DISPOSITION) {
                            static const QByteArray FORM_DATA    { QB ("form-data;") };
                            static const QByteArray KEY_FILENAME { QB ("filename") };
                            static const QByteArray KEY_NAME     { QB ("name") };
                            if (headerVal.startsWith (FORM_DATA)) {
                                const QByteArrayList subparts { headerVal.split (';') };
                                for (const QByteArray & tmp : subparts) {
                                    const qoffset posEquals { tmp.indexOf (EQUAL) };
                                    if (posEquals > 0) {
                                        const QByteArray subKey { tmp.left (posEquals).trimmed ().toLower () };
                                        const QByteArray subVal { unquote (tmp.mid (posEquals +1).trimmed ()) };
                                        if (subKey == KEY_NAME) {
                                            part.formDataName = subVal;
                                        }
                                        else if (subKey == KEY_FILENAME) {
                                            part.formDataFilename = subVal;
                                        }
                                        else { }
                                    }
                                }
                            }
                        }
                        status = WAIT_HEADERS;
                    }
                    else {
                        status = ERROR;
                        break;
                    }
                }
                else if (posCrlf == offset) { // end of headers
                    offset = (posCrlf +2);
                    status = WAIT_DATA;
                }
                else {
                    status = ERROR;
                }
            }
            if (status == WAIT_DATA) {
                const qoffset posBound { data.indexOf (boundary, offset) };
                if (posBound >= offset) {
                    part.data = data.mid (offset, (posBound - offset - CRLF ().size ()));
                    offset = posBound;
                    status = WAIT_DELIMITER;
                }
                else { // no data
                    status = ERROR;
                }
            }
            if (status != ERROR) {
                ret.append (part);
            }
            else {
                break;
            }
        }
    }
    return (status == DONE ? ret : QVector<DataPart> { });
}

void QtHTTP::AbstractFrame::fromHtml (const QString & html) {
    headers.insert (Headers.CONTENT_TYPE, MIMETYPE_HTML ());
    data = html.toUtf8 ();
}

void AbstractFrame::fromJson (const QJsonDocument & json) {
    headers.insert (Headers.CONTENT_TYPE, MIMETYPE_JSON ());
    data = json.toJson (QJsonDocument::Compact);
}

void AbstractFrame::fromUrlEncoded (const QStringHash & args) {
    headers.insert (Headers.CONTENT_TYPE, MIMETYPE_URL ());
    QUrlQuery query { };
    for (QStringHash::const_iterator it { args.constBegin () }, end { args.constEnd () }; it != end; ++it) {
        query.addQueryItem (it.key (), it.value ());
    }
    data = query.toString (QUrl::FullyEncoded).toLatin1 ();
}

void AbstractFrame::fromMultiPart (const QVector<DataPart> & parts) {
    static const QByteArray boundary {
        (QB ("###") % QDateTime::currentDateTimeUtc ().toString ().toLatin1 ().toBase64 () % QB ("###"))
    };
    headers.insert (Headers.CONTENT_TYPE, (MIMETYPE_MULTI () % SEMICOLON % SPACE % BOUNDARY () % QUOTE % boundary % QUOTE));
    data.clear ();
    for (const DataPart & tmp : parts) {
        data += DOUBLE_DASH ();
        data += boundary;
        data += CRLF ();
        for (QByteArrayHash::const_iterator it { tmp.headers.constBegin () }, end { tmp.headers.constEnd () }; it != end; ++it) {
            data += it.key ();
            data += COLON;
            data += SPACE;
            data += it.value ();
            data += CRLF ();
        }
        data += CRLF ();
        data += tmp.data;
        data += CRLF ();
    }
    data += DOUBLE_DASH ();
    data += boundary;
    data += CRLF ();
    data += DOUBLE_DASH ();
}

}
