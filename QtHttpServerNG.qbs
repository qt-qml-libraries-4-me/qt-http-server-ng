import qbs;

Project {
    name: "Qt5 HTTP Server NG";
    minimumQbsVersion: "1.7.0";
    references: [];

    Product {
        name: "app-qt-http-server-ng";
        type: "application";
        targetName: "QtHttpServerNG";
        cpp.rpaths: ["$ORIGIN", "$ORIGIN/lib"];
        cpp.cxxLanguageVersion: "c++14";

        Depends {
            name: "cpp";
        }
        Depends {
            name: "Qt";
            submodules: ["core", "network"];
        }
        Group {
            name: "C++ files";
            files: [
                "QtHttpServer.cpp",
                "QtHttpServer.hpp",
                "main.cpp",
            ]
        }
        Group {
            qbs.install: true;
            fileTagsFilter: product.type;
        }
    }
}
