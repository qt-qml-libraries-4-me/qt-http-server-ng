
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QMimeDatabase>
#include <QUrl>

#include "QtHttpServer.hpp"

int main (int argc, char * argv []) {
    QCoreApplication app { argc, argv };
    QtHTTP::Server srv { };
    QObject::connect (&srv, &QtHTTP::Server::started, &srv, [] (const quint16 port) {
        qInfo () << "SERVER STARTED ON PORT :" << port;
    });
    QObject::connect (&srv, &QtHTTP::Server::stopped, &srv, [] (void) {
        qInfo () << "SERVER STOPPED";
    });
    QObject::connect (&srv, &QtHTTP::Server::connected, &srv, [] (QtHTTP::Client * client) {
        qInfo () << "CLIENT CONNECTED :" << client;
    });
    QObject::connect (&srv, &QtHTTP::Server::disconnected, &srv, [] (QtHTTP::Client * client) {
        qInfo () << "CLIENT DISCONNECTED :" << client;
    });
    QObject::connect (&srv, &QtHTTP::Server::requested, &srv, [] (QtHTTP::Client * client, const QtHTTP::Request * request, QtHTTP::Reply * reply) {
        qInfo () << "REQUEST NEED REPLY :" << client << qPrintable (request->cmd) << qPrintable (request->url) << qPrintable (request->version);
        if (request->url == QB ("/test")) {
            if (request->cmd == QtHTTP::Commands.GET) {
                reply->status = QtHTTP::StatusCodes::OK;
                reply->compress = QtHTTP::Compression::FORCED;
                reply->data = QB ("<html>"
                                  "<body>"
                                  "<form method='post' action='/test' enctype='multipart/form-data'>"
                                  "<input type='text' name='foo' value='123' />"
                                  "<input type='text' name='bar' value='blah' />"
                                  "<input type='file' name='fs' />"
                                  "<input type='submit' value='OK' />"
                                  "</form>"
                                  "</body>"
                                  "</html>");
            }
            else if (request->cmd == QtHTTP::Commands.POST) {
                reply->status = QtHTTP::StatusCodes::OK;
                const QByteArray type { request->headers.value (QtHTTP::Headers.CONTENT_TYPE) };
                if (type.startsWith (QB ("application/x-www-form-urlencoded"))) {
                    const QtHTTP::QStringHash args { request->toUrlEncoded () };
                    for (QtHTTP::QStringHash::const_iterator it { args.constBegin () }, end { args.constEnd () }; it != end; ++it) {
                        qInfo () << "URL ENCODED ARG :" << it.key () << it.value ();
                    }
                }
                else if (type.startsWith (QB ("multipart/form-data"))) {
                    const QVector<QtHTTP::DataPart> parts { request->toMultiPart () };
                    for (const QtHTTP::DataPart & tmp : parts) {
                        qInfo () << "MULTI-PART DATA :" << tmp.headers << tmp.data.size () << "bytes";
                    }
                }
                reply->data = QB ("OK");
            }
            else {
                reply->status = QtHTTP::StatusCodes::METHOD_NOT_ALLOWED;
            }
        }
        else {
            QFile file { QS ("%1%2").arg (QDir::homePath (), QUrl { QString::fromLatin1 (request->url) }.toString (QUrl::PrettyDecoded)) };
            if (file.open (QFile::ReadOnly)) {
                static QMimeDatabase MIME_DB { };
                reply->status = QtHTTP::StatusCodes::OK;
                reply->compress = QtHTTP::Compression::AUTO;
                reply->headers.insert (QtHTTP::Headers.CONTENT_TYPE, MIME_DB.mimeTypeForFile (file.fileName ()).name ().toLatin1 ());
                reply->data = file.readAll ();
                file.close ();
            }
            else {
                reply->status = QtHTTP::StatusCodes::NOT_FOUND;
            }
        }
    });
    srv.start (12345);
    return QCoreApplication::exec ();
}
